Source: haskell-sdl-gfx
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Miriam Ruiz <little_miry@yahoo.es>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 alex,
 c2hs,
 cdbs,
 cpphs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-sdl-dev,
 libghc-sdl-prof,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libsdl-gfx1.2-dev,
 libsdl1.2-dev,
 libx11-dev,
 pkg-config,
Build-Depends-Indep: ghc-doc,
 libghc-sdl-doc,
Standards-Version: 4.1.4
Homepage: https://hackage.haskell.org/package/SDL-gfx
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-sdl-gfx
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-sdl-gfx]

Package: libghc-sdl-gfx-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL gfx binding for GHC
 This package provides the SDL gfx library bindings for the Haskell
 programming language. SDL gfx is an extension to the SDL library which
 provides basic antialiased drawing routines such as lines, circles or
 polygons, an interpolating rotozoomer for SDL surfaces, framerate control
 and MMX image filters.

Package: libghc-sdl-gfx-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL gfx binding for GHC - profiling libraries
 This package provides the SDL gfx library bindings for the Haskell
 programming language, compiled for profiling. SDL gfx is an extension
 to the SDL library which provides basic antialiased drawing routines
 such as lines, circles or polygons, an interpolating rotozoomer for SDL
 surfaces, framerate control and MMX image filters.

Package: libghc-sdl-gfx-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell SDL gfx binding for GHC - documentation
 This package provides the documentation for the SDL gfx library bindings
 for the Haskell programming language. SDL gfx is an extension to the SDL
 library which provides basic antialiased drawing routines such as lines,
 circles or polygons, an interpolating rotozoomer for SDL surfaces,
 framerate control and MMX image filters.
