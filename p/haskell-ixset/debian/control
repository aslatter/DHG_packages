Source: haskell-ixset
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Giovanni Mascellani <gio@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-safecopy-dev,
 libghc-safecopy-prof,
 libghc-syb-dev,
 libghc-syb-prof,
 libghc-syb-with-class-dev (>= 0.6.1),
 libghc-syb-with-class-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-safecopy-doc,
 libghc-syb-doc,
 libghc-syb-with-class-doc,
Standards-Version: 4.1.4
Homepage: http://happstack.com
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-ixset
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-ixset]

Package: libghc-ixset-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell implementation of queryable sets - GHC libraries${haskell:ShortBlurb}
 IxSet is a Haskell efficient implementation of queryable sets: it
 makes you able to define complex data types and index them on some of
 theirs fields. Then you can efficiently query the IxSet to find the
 records that satisfy some condition.
 .
 ${haskell:Blurb}

Package: libghc-ixset-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell implementation of queryable sets - GHC profiling libraries${haskell:ShortBlurb}
 IxSet is a Haskell efficient implementation of queryable sets: it
 makes you able to define complex data types and index them on some of
 theirs fields. Then you can efficiently query the IxSet to find the
 records that satisfy some condition.
 .
 ${haskell:Blurb}

Package: libghc-ixset-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell implementation of queryable sets - documentation${haskell:ShortBlurb}
 IxSet is a Haskell efficient implementation of queryable sets: it
 makes you able to define complex data types and index them on some of
 theirs fields. Then you can efficiently query the IxSet to find the
 records that satisfy some condition.
 .
 ${haskell:Blurb}
