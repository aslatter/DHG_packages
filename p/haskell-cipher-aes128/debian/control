Source: haskell-cipher-aes128
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-cereal-dev,
 libghc-cereal-prof,
 libghc-crypto-api-dev (>= 0.13),
 libghc-crypto-api-prof,
 libghc-tagged-dev,
 libghc-tagged-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-cereal-doc,
 libghc-crypto-api-doc,
 libghc-tagged-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/TomMD/cipher-aes128
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-cipher-aes128
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-cipher-aes128]
X-Description: AES and common modes
 Cipher-aes128 is an implementation of AES and common modes of operation.
 .
 It borrows Hanquez's C AES code (see 'cipher-aes') but is unique due to
   * including compile-time detection of NI compiler support,
   * a slightly more functional interface for GCM operations,
   * exposure of 'Ptr' based operations via the .Internal module, and
   * build-in crypto-api support.
 Cipher-aes128 was originally developed as "'cipher-aes' plus trampolines",
 which has since been adopted into cipher-aes.

Package: libghc-cipher-aes128-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cipher-aes128-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-cipher-aes128-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
