haskell-lambdahack (0.8.3.0-4) unstable; urgency=medium

  * Remove build dependency on libghc-text-dev (provided by ghc-8.4.3)
  * Remove build dependency on libghc-stm-dev (provided by ghc-8.4.3)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 01 Oct 2018 17:50:18 +0300

haskell-lambdahack (0.8.3.0-3) unstable; urgency=medium

  * Sourceful upload for ghc-8.4.3 (haddock-interface-33)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:13:37 +0300

haskell-lambdahack (0.8.3.0-2) unstable; urgency=medium

  * Turn off optimization for architectures that take forever compiling
  * Install fonts in usr/share/lambdahack, where the binary expects them

 -- Mikolaj Konarski <mikolaj.konarski@funktory.com>  Mon, 02 Jul 2018 17:26:16 +0200

haskell-lambdahack (0.8.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Mikolaj Konarski <mikolaj.konarski@funktory.com>  Sat, 30 Jun 2018 13:17:24 +0200

haskell-lambdahack (0.7.1.0-9) unstable; urgency=medium

  * Build-depend on sdl instead of vty, for better UI
  * Set -O0 also for other architectures that fail with OOM
  * Update Vcs-Browser field
  * Update Depends field to fix lintian/missing-depends-line

 -- Mikolaj Konarski <mikolaj.konarski@funktory.com>  Fri, 08 Jun 2018 23:29:25 +0200

haskell-lambdahack (0.7.1.0-8) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Mikolaj Konarski ]
  * Update copyright information
  * Update package description and homepage
  * Remove spurious dependency on gtk
  * Condense X-Description to satisfy lintian

 -- Clint Adams <clint@debian.org>  Sun, 06 May 2018 22:10:02 -0400

haskell-lambdahack (0.7.1.0-6) unstable; urgency=medium

  * Restore maximum stack size (for mips64el and s390x).
  * Use -O0 on armel/armhf.

 -- Clint Adams <clint@debian.org>  Sun, 29 Apr 2018 10:01:26 -0400

haskell-lambdahack (0.7.1.0-5) unstable; urgency=medium

  * Reduce maximum stack size.

 -- Clint Adams <clint@debian.org>  Fri, 27 Apr 2018 08:52:14 -0400

haskell-lambdahack (0.7.1.0-4) unstable; urgency=medium

  * Install the right files.

 -- Clint Adams <clint@debian.org>  Sat, 21 Apr 2018 19:45:11 -0400

haskell-lambdahack (0.7.1.0-3) unstable; urgency=medium

  * Force the vty build.

 -- Clint Adams <clint@debian.org>  Sat, 21 Apr 2018 18:32:56 -0400

haskell-lambdahack (0.7.1.0-2) unstable; urgency=medium

  * Build-depend on vty instead of sdl.

 -- Clint Adams <clint@debian.org>  Sat, 21 Apr 2018 16:13:57 -0400

haskell-lambdahack (0.7.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 21:53:28 -0400

haskell-lambdahack (0.5.0.0-3) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:55 -0400

haskell-lambdahack (0.5.0.0-2) unstable; urgency=medium

  * Don't use -N1 for testsuite.  closes: #864854.

 -- Clint Adams <clint@debian.org>  Mon, 19 Jun 2017 13:47:56 -0400

haskell-lambdahack (0.5.0.0-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Mon, 12 Jun 2017 07:52:36 -0400
