Source: yi
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Marcel Fourne <debian@marcelfourne.de>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 alex (>= 3.0.3),
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 haskell-devscripts (>= 0.13),
 libghc-microlens-platform-dev (>= 0.3.4.0),
 libghc-optparse-applicative-dev (>= 0.13.0.0),
 libghc-yi-core-dev (>= 0.18),
 libghc-yi-frontend-vty-dev (>= 0.18),
 libghc-yi-keymap-emacs-dev (>= 0.18),
 libghc-yi-keymap-vim-dev (>= 0.18),
 libghc-yi-misc-modes-dev (>= 0.18),
 libghc-yi-mode-haskell-dev (>= 0.18),
 libghc-yi-mode-javascript-dev (>= 0.18),
 libghc-yi-rope-dev (>= 0.10),
Standards-Version: 4.1.4
Homepage: https://github.com/yi-editor/yi#readme
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/yi
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/yi]

Package: yi
Architecture: any
Section: editors
Depends:
 libghc-yi-frontend-pango-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Haskell-Scriptable Editor
 Yi is a text editor written in Haskell and extensible in Haskell.  The goal
 of the Yi project is to provide a flexible, powerful, and correct editor
 for haskell hacking.
 .
 This package comes pre-configured with the default configuration. If
 you want to build your custom-configured version, make sure that
 libghc-yi-core-dev is installed and put your configuration in
 ~/.config/yi/yi.hs
