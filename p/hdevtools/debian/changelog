hdevtools (0.1.8.0-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Add missing "Upstream-Name" field into "debian/copyright".

  [ Gianfranco Costamagna ]
  * New upstream release

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 24 Aug 2019 18:45:04 +0200

hdevtools (0.1.7.0-1) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10
  * New upstream release

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sat, 29 Sep 2018 19:35:51 +0300

hdevtools (0.1.6.1-2) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:12 -0400

hdevtools (0.1.6.1-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 07 Jan 2018 15:18:00 -0500

hdevtools (0.1.6.0-1) unstable; urgency=medium

  * New upstream release

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 03 Sep 2017 14:27:25 -0700

hdevtools (0.1.5.0-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 18 Jun 2017 02:40:56 -0400

hdevtools (0.1.4.1-2) unstable; urgency=medium

  * Apply upstream patch that fixes ghc library directory lookup
    via stack for system installed ghc (Closes: #846023).
  * Remove duplicate entries from build-depends field.
  * Remove Joey Hess as an uploader (he has left Debian), and add myself.

 -- Ilias Tsitsimpis <i.tsitsimpis@gmail.com>  Mon, 28 Nov 2016 13:24:15 +0200

hdevtools (0.1.4.1-1) unstable; urgency=medium

  * New upstream version.
  * Update Vcs-* fields to reflect repository move.

 -- Clint Adams <clint@debian.org>  Sat, 29 Oct 2016 13:29:42 -0400

hdevtools (0.1.3.1-2) unstable; urgency=medium

  * Update Vcs-* fields.

 -- Clint Adams <clint@debian.org>  Sat, 29 Oct 2016 12:12:28 -0400

hdevtools (0.1.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 12 Jun 2016 14:04:41 +0200

hdevtools (0.1.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 17 Feb 2016 10:56:08 +0100

hdevtools (0.1.2.1-1) unstable; urgency=medium

  * New upstream release (Closes: #808550)
  * Add the usual rpath lintian override
  * Build using haskell-devscripts, to get the benefit of
    ${haskell:ghc-version}. We need to depend on that, as hdevtools is very
    much tied to the GHC version (cf. ghc-mod, same situation).

 -- Joachim Breitner <nomeata@debian.org>  Sun, 20 Dec 2015 22:42:17 +0100

hdevtools (0.1.1.9-1) unstable; urgency=medium

  * New upstream release
    Requires ghc-7.10, due to updated dependency on process

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 Jul 2015 15:34:17 +0200

hdevtools (0.1.0.9-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 01 Jun 2015 21:25:04 +0200

hdevtools (0.1.0.8-2) experimental; urgency=medium

  * Remove debian/docs, not shipped by upstream any more

 -- Joachim Breitner <nomeata@debian.org>  Tue, 14 Apr 2015 13:43:50 +0200

hdevtools (0.1.0.8-1) experimental; urgency=medium

  * New upstream release
  * Fix watchfile 
  * Switch to 3.0 (quilt) 
  * Add debian/source/options to ignore files from upstream git that are not
    in the released tarball.

 -- Joachim Breitner <nomeata@debian.org>  Tue, 14 Apr 2015 12:48:36 +0200

hdevtools (0.1.0.5-3) unstable; urgency=low

  [ Colin Watson ]
  * Build-depend on ghc-ghci, as Info.getIdentifierInfo relies on
    interpreter-specific functions.

  [ Joey Hess ]
  * Rebuilt against current version of libghc-ghc-paths-dev,
    which is necessary for it to be able to load modules built with ghc
    7.6.3.

 -- Joey Hess <joeyh@debian.org>  Sat, 27 Jul 2013 13:01:31 -0400

hdevtools (0.1.0.5-2) unstable; urgency=low

  * Correct debian/copyright file.

 -- Joey Hess <joeyh@debian.org>  Sun, 07 Apr 2013 17:02:19 -0400

hdevtools (0.1.0.5-1) unstable; urgency=low

  * Initial release.

 -- Joey Hess <joeyh@debian.org>  Sun, 07 Apr 2013 14:48:20 -0400
