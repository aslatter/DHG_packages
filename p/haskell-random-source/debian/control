Source: haskell-random-source
Section: haskell
Priority: optional
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-ghci,
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-flexible-defaults-dev (>= 0.0.0.2),
 libghc-flexible-defaults-prof,
 libghc-mersenne-random-pure64-dev,
 libghc-mersenne-random-pure64-prof,
 libghc-mwc-random-dev,
 libghc-mwc-random-prof,
 libghc-random-dev,
 libghc-random-prof,
 libghc-stateref-dev (<< 0.4),
 libghc-stateref-dev (>= 0.3),
 libghc-stateref-prof,
 libghc-syb-dev,
 libghc-syb-prof,
 libghc-th-extras-dev,
 libghc-th-extras-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-flexible-defaults-doc,
 libghc-mersenne-random-pure64-doc,
 libghc-mwc-random-doc,
 libghc-random-doc,
 libghc-stateref-doc,
 libghc-syb-doc,
 libghc-th-extras-doc,
Standards-Version: 4.1.4
Homepage: http://hackage.haskell.org/package/random-source
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-random-source]
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-random-source
X-Description: generic basis for random number generators
 Random number generation based on entropy sources able to produce a
 small but well-defined set of primitive variates. Also includes
 facilities for "completing" partial implementations, making it easy
 to define new entropy sources in a way that is naturally
 forward-compatible.

Package: libghc-random-source-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-random-source-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-random-source-doc
Section: doc
Architecture: all
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
