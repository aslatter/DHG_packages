Source: haskell-servant
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 haskell-devscripts (>= 0.15),
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-quickcheck2-dev (>= 2.12.6.1),
 libghc-quickcheck2-dev (<< 2.13),
 libghc-quickcheck2-prof,
 libghc-aeson-dev (>= 1.4.1.0),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-attoparsec-dev (>= 0.13.2.2),
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-prof,
 libghc-base-compat-dev (>= 0.10.5),
 libghc-base-compat-dev (<< 0.11),
 libghc-base-compat-prof,
 libghc-bifunctors-dev (>= 5.5.3),
 libghc-bifunctors-dev (<< 5.6),
 libghc-bifunctors-prof,
 libghc-case-insensitive-dev (>= 1.2.0.11),
 libghc-case-insensitive-dev (<< 1.3),
 libghc-case-insensitive-prof,
 libghc-http-api-data-dev (>= 0.4),
 libghc-http-api-data-dev (<< 0.4.1),
 libghc-http-api-data-prof,
 libghc-http-media-dev (>= 0.7.1.3),
 libghc-http-media-dev (<< 0.8),
 libghc-http-media-prof,
 libghc-http-types-dev (>= 0.12.2),
 libghc-http-types-dev (<< 0.13),
 libghc-http-types-prof,
 libghc-mmorph-dev (>= 1.1.2),
 libghc-mmorph-dev (<< 1.2),
 libghc-mmorph-prof,
 libghc-network-uri-dev (>= 2.6.1.0),
 libghc-network-uri-dev (<< 2.7),
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-prof,
 libghc-singleton-bool-dev (>= 0.1.4),
 libghc-singleton-bool-dev (<< 0.1.5),
 libghc-singleton-bool-prof,
 libghc-string-conversions-dev (>= 0.4.0.1),
 libghc-string-conversions-dev (<< 0.5),
 libghc-string-conversions-prof,
 libghc-tagged-dev (>= 0.8.6),
 libghc-tagged-dev (<< 0.9),
 libghc-tagged-prof,
 libghc-vault-dev (>= 0.3.1.2),
 libghc-vault-dev (<< 0.4),
 libghc-vault-prof,
 libghc-aeson-dev,
 libghc-base-compat-dev,
 libghc-doctest-dev (>= 0.16.0),
 libghc-doctest-dev (<< 0.17),
 libghc-doctest-prof,
 libghc-hspec-dev (>= 2.6.0),
 libghc-hspec-dev (<< 2.7),
 libghc-hspec-prof,
 libghc-quickcheck-instances-dev (>= 0.3.19),
 libghc-quickcheck-instances-dev (<< 0.4),
 libghc-string-conversions-dev,
 libghc-cabal-doctest-dev (>= 1.0),
Build-Depends-Indep: ghc-doc,
 libghc-quickcheck2-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-base-compat-doc,
 libghc-bifunctors-doc,
 libghc-case-insensitive-doc,
 libghc-http-api-data-doc,
 libghc-http-media-doc,
 libghc-http-types-doc,
 libghc-mmorph-doc,
 libghc-network-uri-doc,
 libghc-singleton-bool-doc,
 libghc-string-conversions-doc,
 libghc-tagged-doc,
 libghc-vault-doc,
Standards-Version: 4.4.0
Homepage: http://haskell-servant.readthedocs.org/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-servant
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-servant]
X-Description: family of combinators for defining webservices APIs
 A family of combinators for defining webservices APIs and serving them.

Package: libghc-servant-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-servant-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-servant-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
