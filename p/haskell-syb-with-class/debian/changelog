haskell-syb-with-class (0.6.1.10-1) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10
  * New upstream release

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 11:19:35 +0300

haskell-syb-with-class (0.6.1.9-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 17:11:25 -0400

haskell-syb-with-class (0.6.1.7-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:36:20 -0400

haskell-syb-with-class (0.6.1.7-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Fri, 14 Oct 2016 14:19:45 -0400

haskell-syb-with-class (0.6.1.7-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 16:49:16 -0400

haskell-syb-with-class (0.6.1.6-3) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:55:07 -0500

haskell-syb-with-class (0.6.1.6-2) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:47 +0200

haskell-syb-with-class (0.6.1.6-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 29 Jun 2015 13:32:31 +0200

haskell-syb-with-class (0.6.1.5-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:53:54 +0200

haskell-syb-with-class (0.6.1.5-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:12:17 +0100

haskell-syb-with-class (0.6.1.5-1) unstable; urgency=low

  * Adjust watch file to new hackage layout
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 04 Aug 2014 23:57:39 +0200

haskell-syb-with-class (0.6.1.4-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:52:10 +0200

haskell-syb-with-class (0.6.1.4-1) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Fri, 19 Oct 2012 22:35:41 +0200

haskell-syb-with-class (0.6.1.3-1) unstable; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Tue, 07 Feb 2012 20:07:47 +0100

haskell-syb-with-class (0.6.1.1-1) unstable; urgency=low

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ Joachim Breitner ]
  * Build-Depend on ghc-ghci, to only attempt builds on architectures
    where template haskell is available.
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 20 Apr 2011 22:38:24 +0530

haskell-syb-with-class (0.6.1-2) unstable; urgency=low

  * debian/source/format: Use 3.0 (quilt).
  * debian/control: description fixed and -doc package added
  * debian/copyright: (C) -> © (lintian warning fixed)

 -- Giovanni Mascellani <gio@debian.org>  Tue, 08 Jun 2010 14:09:37 +0200

haskell-syb-with-class (0.6.1-1) unstable; urgency=low

  * Adopt by the Debian Haskell Group
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Feb 2010 20:22:41 +0100

haskell-syb-with-class (0.5.1-2) unstable; urgency=low

  * Added missing build dep ghc6-prof.

 -- Kari Pahula <kaol@debian.org>  Wed, 04 Mar 2009 21:45:25 +0200

haskell-syb-with-class (0.5.1-1) unstable; urgency=low

  * New upstream release
  * Use hlibrary.mk from haskell-devscripts.
  * Added a -prof package.

 -- Kari Pahula <kaol@debian.org>  Fri, 27 Feb 2009 18:39:16 +0200

haskell-syb-with-class (0.4~darcs20080112-2) unstable; urgency=low

  * Copied newest hlibrary.mk CDBS rule file.
  * Added ${haskell:Depends} as a dependency.  (Closes: #502756)

 -- Kari Pahula <kaol@debian.org>  Sun, 19 Oct 2008 21:07:03 +0300

haskell-syb-with-class (0.4~darcs20080112-1) unstable; urgency=low

  * Initial release (Closes: #432794)

 -- Kari Pahula <kaol@debian.org>  Mon, 03 Mar 2008 19:09:15 +0200
