Source: ghc-mod
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-cabal-helper-dev (>= 0.7.3.0-3),
 libghc-cabal-helper-dev (<< 0.8),
 libghc-cabal-helper-prof,
 libghc-djinn-ghc-dev (<< 0.1),
 libghc-djinn-ghc-dev (>= 0.0.2.2),
 libghc-djinn-ghc-prof,
 libghc-extra-dev (>= 1.4),
 libghc-extra-dev (<< 1.6),
 libghc-extra-prof,
 libghc-fclabels-dev (<< 2.1),
 libghc-fclabels-dev (>= 2.0),
 libghc-fclabels-prof,
 libghc-ghc-paths-dev (>= 0.1.0.9),
 libghc-ghc-paths-dev (<< 0.2),
 libghc-ghc-paths-prof,
 libghc-ghc-syb-utils-dev (>= 0.2.3),
 libghc-ghc-syb-utils-dev (<< 0.3),
 libghc-ghc-syb-utils-prof,
 libghc-src-exts-dev (>= 1.18),
 libghc-src-exts-dev (<< 1.20),
 libghc-src-exts-prof,
 libghc-hlint-dev (>= 2.0.8),
 libghc-hlint-dev (<< 2.1),
 libghc-hlint-prof,
 libghc-monad-control-dev (<< 1.1),
 libghc-monad-control-dev (>= 1),
 libghc-monad-control-prof,
 libghc-monad-journal-dev (>= 0.4),
 libghc-monad-journal-dev (<< 0.8),
 libghc-monad-journal-prof,
 libghc-old-time-dev,
 libghc-old-time-prof,
 libghc-optparse-applicative-dev (>= 0.13.0.0),
 libghc-optparse-applicative-dev (<< 0.14),
 libghc-optparse-applicative-prof,
 libghc-pipes-dev (>= 4.1),
 libghc-pipes-dev (<< 4.4),
 libghc-pipes-prof,
 libghc-safe-dev (<< 0.4),
 libghc-safe-dev (>= 0.3.9),
 libghc-safe-prof,
 libghc-semigroups-dev (>= 0.10.0),
 libghc-semigroups-dev (<< 0.19),
 libghc-semigroups-prof,
 libghc-split-dev (>= 0.2.2),
 libghc-split-dev (<< 0.3),
 libghc-split-prof,
 libghc-syb-dev (>= 0.5.1),
 libghc-syb-dev (<< 0.8),
 libghc-syb-prof,
 libghc-temporary-dev (>= 1.2.0.3),
 libghc-temporary-dev (<< 1.3),
 libghc-temporary-prof,
 libghc-transformers-base-dev (>= 0.4.4),
 libghc-transformers-base-dev (<< 0.5),
 libghc-transformers-base-prof,
Build-Depends-Indep: ghc-doc,
 libghc-cabal-helper-doc,
 libghc-djinn-ghc-doc,
 libghc-extra-doc,
 libghc-fclabels-doc,
 libghc-ghc-paths-doc,
 libghc-ghc-syb-utils-doc,
 libghc-hlint-doc,
 libghc-monad-control-doc,
 libghc-monad-journal-doc,
 libghc-old-time-doc,
 libghc-optparse-applicative-doc,
 libghc-pipes-doc,
 libghc-safe-doc,
 libghc-semigroups-doc,
 libghc-split-doc,
 libghc-src-exts-doc,
 libghc-syb-doc,
 libghc-temporary-doc,
 libghc-transformers-base-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/DanielG/ghc-mod
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/ghc-mod
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/ghc-mod]

Package: ghc-mod
Architecture: any
Depends:
 cabal-helper,
 ${haskell:ghc-version},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ghc-mod-el (= ${binary:Version}),
Description: Happy Haskell programming
 This package provides the executables ghc-mod and ghc-modi. The GNU Emacs
 integration code is provided by a separate package, namely ghc-mod-el.

Package: ghc-mod-el
Architecture: all
Depends:
 emacsen-common (>= 2.0.8),
 ghc-mod (<< ${source:Version}.1~),
 ghc-mod (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 haskell-mode,
Breaks:
 ghc-mod (<< 5.2.1.1-1),
Replaces:
 ghc-mod (<< 5.2.1.1-1),
Description: Happy Haskell programming with Emacs
 This package proviceds the GNU Emacs integration code for ghc-mod.
 .
 Note: This package does NOT support xemacs*. Only for emacs*.
